describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('LogIn Screen', async () => {
    await element(by.id('email')).replaceText('pkgupta@ucdavis.edu');
    await element(by.id('password')).replaceText('secret1!');
    await element(by.id('logIn')).tap();
  });
});
